#include <gtest/gtest.h>

#include "include/TTabRecord.h"

TEST(BakalinKazakov_TTabRecord, Can_Create_Tab_Record) {
  ASSERT_NO_THROW(TTabRecord tab_record("", nullptr));
}

TEST(BakalinKazakov_TTabRecord, Can_Assign_To_Itself) {
  TTabRecord tab_record("", nullptr);

  ASSERT_NO_THROW(tab_record = tab_record);
}

TEST(BakalinKazakov_TTabRecord, Can_Assign_Tab_Record) {
  TTabRecord tab_record1("1", nullptr);

  TTabRecord tab_record2 = tab_record1;

  EXPECT_EQ(tab_record1, tab_record2);
}

TEST(BakalinKazakov_TTabRecord, Can_Get_Key) {
  TTabRecord tab_record("1", nullptr);

  EXPECT_EQ(tab_record.GetKey(), "1");
}

TEST(BakalinKazakov_TTabRecord, Can_Set_Key) {
  TTabRecord tab_record("1", nullptr);

  tab_record.SetKey("2");

  EXPECT_EQ(tab_record.GetKey(), "2");
}

TEST(BakalinKazakov_TTabRecord, Can_Get_Value) {
  TTabRecord tab_record("1", nullptr);

  EXPECT_EQ(tab_record.GetValue(), nullptr);
}

TEST(BakalinKazakov_TTabRecord, Can_Set_Value) {
  TTabRecord tab_record("1", nullptr);

  tab_record.SetValue(&tab_record);

  EXPECT_EQ(tab_record.GetValue(), &tab_record);
}

TEST(BakalinKazakov_TTabRecord, Can_Get_Copy) {
  TTabRecord tab_record1("1", nullptr);

  TDatValue* tab_record2 = tab_record1.GetCopy();
  tab_record1.SetKey("2");

  EXPECT_EQ(((TTabRecord*)tab_record2)->GetKey(), "1");
}

TEST(BakalinKazakov_TTabRecord, Can_Compare_Two_Equal_Records) {
  TTabRecord tab_record1("1", nullptr), tab_record2("1", nullptr);

  EXPECT_EQ(tab_record1 == tab_record2, true);
}

TEST(BakalinKazakov_TTabRecord, Can_Compare_Two_Different_Records) {
  TTabRecord tab_record1("1", nullptr), tab_record2("2", nullptr);

  EXPECT_EQ(tab_record1 == tab_record2, false);
}

TEST(BakalinKazakov_TTabRecord, Can_Compare_For_Less) {
  TTabRecord tab_record1 = TTabRecord("ab", nullptr);
  TTabRecord tab_record2 = TTabRecord("cd", nullptr);

  EXPECT_EQ(tab_record1 < tab_record2, true);
}

TEST(BakalinKazakov_TTabRecord, Can_Compare_For_More) {
  TTabRecord tab_record1 = TTabRecord("cd", nullptr);
  TTabRecord tab_record2 = TTabRecord("ab", nullptr);

  EXPECT_EQ(tab_record1 > tab_record2, true);
}
