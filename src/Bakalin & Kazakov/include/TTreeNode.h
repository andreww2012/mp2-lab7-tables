#pragma once

#include "include/TTabRecord.h"

#include <string>

// Абстрактный базовый класс объектов-значений для деревьев
class TTreeNode : public TTabRecord {
 public:
  TTreeNode(const std::string key = "", TDatValue* value = nullptr,
  TTreeNode* left = nullptr, TTreeNode* right = nullptr);

  TTreeNode* GetLeft(void) const;
  TTreeNode* GetRight(void) const;

  virtual TDatValue* GetCopy(void) override;

 protected:
  TTreeNode *node_left_, *node_right_;

  friend class TTreeTable;
  friend class TBalanceTree;
};
