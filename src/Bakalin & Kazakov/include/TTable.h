#pragma once

#include "include/TDatValue.h"

#include <string>
#include <iostream>

// Абстрактный базовый класс, содержащий спецификацию методов таблицы
class TTable {
 public:
  enum Position {
    FIRST_POS,
    CURRENT_POS,
    LAST_POS
  };

  TTable(void) : records_amount_(0), efficiency_(0) {}
  virtual ~TTable(void) {}

  int GetRecordsAmount(void) const { return records_amount_; }
  int GetEfficiency(void) const { return efficiency_; }

  bool IsEmpty(void) const { return records_amount_ == 0; }
  virtual bool IsFull(void) const = 0;

  virtual std::string GetKey(Position mode = Position::CURRENT_POS) const = 0;
  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS) const = 0;

  virtual TDatValue* FindRecord(const std::string& key) = 0;
  virtual void InsRecord(const std::string& key, TDatValue* value) = 0;
  virtual void DelRecord(const std::string& key) = 0;

  virtual void Reset(void) = 0;
  virtual bool IsTabEnded(void) const = 0;
  virtual void GoNext(void) = 0;

 protected:
  int records_amount_;  // количество записей, но не размер таблицы
  int efficiency_;  // показатель эффективности выполнения операции
};
