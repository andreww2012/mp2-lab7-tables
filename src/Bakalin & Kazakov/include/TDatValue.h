#pragma once

class TDatValue {
public:
  virtual TDatValue* GetCopy(void) = 0;
  ~TDatValue(void) {}
};
