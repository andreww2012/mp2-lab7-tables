#pragma once

#include "include/TArrayTable.h"

#include <string>

// Класс просматриваемых таблиц
class TScanTable : public TArrayTable {
 public:
  explicit TScanTable(const int size = kTabDefaultSize);

  virtual TDatValue* FindRecord(const std::string& key);
  virtual void InsRecord(const std::string& key, TDatValue* value);
  virtual void DelRecord(const std::string& key);
};
