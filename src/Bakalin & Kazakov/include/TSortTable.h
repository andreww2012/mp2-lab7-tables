#pragma once

#include "include/TScanTable.h"

#include <string>

// Класс упорядоченных таблиц
class TSortTable: public TScanTable {
 public:
  enum SortMethod {
    INSERT_SORT,
    MERGE_SORT,
    QUICK_SORT
  };

  explicit TSortTable(const int size = kTabDefaultSize);
  TSortTable(const TScanTable&);
  TSortTable& operator=(const TScanTable&);

  SortMethod GetSortMethod(void) const;
  void SetSortMethod(SortMethod sort_method);

  virtual TDatValue* FindRecord(const std::string& key) override;
  virtual void InsRecord(const std::string& key, TDatValue* value) override;
  virtual void DelRecord(const std::string& key) override;

 protected:
  SortMethod sort_method_;

  void SortData(void);

  static void InsertSort(TTabRecord** records, const int size);

  static void MergeSort(TTabRecord** records, const int size);
  static void MergeSorter(TTabRecord**& data, TTabRecord**& buff,
  const int size);
  static void MergeData(TTabRecord**& data, TTabRecord**& buff,
  const int n1, const int n2);

  // Быстрая сортировка
  static void QuickSort(TTabRecord** records, const int size);
  // Быстрая сортировка - выбор ведущего элемента и разделение данных
  static void QuickSplit(TTabRecord** records, const int size, int& pivot);
};
