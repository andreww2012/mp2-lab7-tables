﻿#include "include/TTreeTable.h"

#include <stack>
#include <string>

TTreeTable::TTreeTable(): TTable() {
  curr_pos_ = 0;
  node_root_ = node_current_ = nullptr;
  node_found_ = nullptr;
}

TTreeTable::~TTreeTable() {
  DeleteTreeTable(node_root_);
}

bool TTreeTable::IsFull() const {
  TTreeNode* node = new TTreeNode();
  bool result = (node == nullptr);
  delete node;

  return result;
}

std::string TTreeTable::GetKey(Position mode) const {
  return (node_current_ == nullptr) ? std::string("") : node_current_->key_;
}

TDatValue* TTreeTable::GetValue(Position mode) const {
  return (node_current_ == nullptr) ? nullptr : node_current_->value_;
}

TDatValue* TTreeTable::FindRecord(const std::string& key) {
  TTreeNode* node = node_root_;
  node_found_ = &node_root_;

  while (node != nullptr) {
    if (node->key_ == key) break;

    if (node->key_ < key) {
      node_found_ = &node->node_right_;
    } else {
      node_found_ = &node->node_left_;
    }

    node = *node_found_;
  }

  return (node == nullptr) ? nullptr : node->value_;
}

void TTreeTable::InsRecord(const std::string& key, TDatValue* value) {
  if (IsFull())
    throw std::string("Table is full.");

  if (FindRecord(key) != nullptr)
    throw std::string("The record already exists.");

  *node_found_ = new TTreeNode(key, value);
  records_amount_++;
}

void TTreeTable::DelRecord(const std::string& key) {
  if (FindRecord(key) == nullptr) {
    throw std::string("Record not found.");
  }
  else {
    TTreeNode *tmp = node_root_;

    while (!iterator_stack_.empty())
      iterator_stack_.pop();

    while (tmp->GetKey() != key) {
      iterator_stack_.push(tmp);

      if (tmp->GetKey() < key)
        tmp = tmp->GetRight();
      else
        tmp = tmp->GetLeft();
    }

    if ((tmp->node_left_ == nullptr) && (tmp->node_right_ == nullptr)) {
      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_right_ = nullptr;
          else if (prev->GetLeft() == tmp)
            prev->node_left_ = nullptr;
        }
      }
      else {
        node_root_ = nullptr;
      }
      delete tmp;
      records_amount_--;
    }
    else if (tmp->node_left_ == nullptr) {
      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_left_ = tmp->node_right_;
          if (prev->GetLeft() == tmp)
            prev->node_left_ = tmp->node_right_;
        }
      }
      else {
        node_root_ = tmp->GetRight();
      }
      delete tmp;
      records_amount_--;
    }
    else if (tmp->node_right_ == nullptr) {
      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_right_ = tmp->node_left_;
          if (prev->GetLeft() == tmp)
            prev->node_left_ = tmp->node_left_;
        }
      }
      else {
        node_root_ = tmp->GetLeft();
      }
      delete tmp;
      records_amount_--;
    }
    else {
      TTreeNode *down_left = tmp->GetRight();
      while (down_left->GetLeft() != nullptr)
        down_left = down_left->node_left_;
      down_left->node_left_ = tmp->GetLeft();

      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_right_ = tmp->node_right_;
          if (prev->GetLeft() == tmp)
            prev->node_left_ = tmp->node_right_;
        }
      }
      else {
        node_root_ = tmp->GetRight();
      }
      delete tmp;
      records_amount_--;
    }
  }
}

void TTreeTable::Reset() {
  TTreeNode* node = node_current_ = node_root_;

  while (!iterator_stack_.empty())
    iterator_stack_.pop();

  curr_pos_ = 0;

  while (node != nullptr) {
    iterator_stack_.push(node);
    node_current_ = node;
    node = node->node_left_;
  }
}

bool TTreeTable::IsTabEnded() const {
  return curr_pos_ >= records_amount_;
}

void TTreeTable::GoNext() {
  records_amount_++;

  if (!IsTabEnded() && (node_current_ != nullptr)) {
    TTreeNode *node = node_current_ = node_current_->GetRight();
    iterator_stack_.pop();

    while (node != nullptr) {
      iterator_stack_.push(node);
      node_current_ = node;
      node = node->GetLeft();
    }

    if ((node_current_ == nullptr) && !iterator_stack_.empty())
      node_current_ = iterator_stack_.top();
  }
}

void TTreeTable::DeleteTreeTable(TTreeNode* node) {
  if (node != nullptr) {
    DeleteTreeTable(node->node_left_);
    DeleteTreeTable(node->node_right_);
    delete node;
  }
}
