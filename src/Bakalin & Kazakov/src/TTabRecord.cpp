#include "include/TTabRecord.h"

#include <string>

TTabRecord::TTabRecord(const std::string key, TDatValue* value) {
  key_ = key;
  value_ = value;
}

TTabRecord& TTabRecord::operator=(TTabRecord& tr) {
  key_ = tr.key_;
  value_ = tr.value_;

  return *this;
}

void TTabRecord::SetKey(const std::string& key) {
  key_ = key;
}

std::string TTabRecord::GetKey() const {
  return key_;
}

void TTabRecord::SetValue(TDatValue* value) {
  value_ = value;
}

TDatValue* TTabRecord::GetValue() const {
  return value_;
}

TDatValue* TTabRecord::GetCopy() {
  return new TTabRecord(key_, value_);
}

bool TTabRecord::operator==(const TTabRecord& tr) const {
  return key_ == tr.key_;
}

bool TTabRecord::operator<(const TTabRecord& tr) const {
  return key_ < tr.key_;
}

bool TTabRecord::operator>(const TTabRecord& tr) const {
  return key_ > tr.key_;
}
