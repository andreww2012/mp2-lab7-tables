#include "include/TArrayTable.h"

#include <string>

TArrayTable::TArrayTable(const int size) {
  if (size <= 0)
    throw std::string("Incorrect size.");

  records_ = new TTabRecord*[size];
  for (int i = 0; i < size; i++) {
    records_[i] = nullptr;
  }

  size_ = size;
  curr_pos_ = 0;
}

TArrayTable::~TArrayTable() {
  for (int i = 0; i < size_; i++) {
    if (records_[i] != nullptr)
      delete records_[i];
  }

  delete[] records_;
}

bool TArrayTable::IsFull() const {
  return records_amount_ >= size_;
}

int TArrayTable::GetTableSize() const {
  return size_;
}

int TArrayTable::GetCurrentPos() const {
  return curr_pos_;
}

void TArrayTable::SetCurrentPos(const int pos) {
  curr_pos_ = ((pos > TArrayTable::kNoRecordsPos)
                && (pos < records_amount_)) ? pos : 0;
}

std::string TArrayTable::GetKey(Position mode) const {
  int pos = TArrayTable::kNoRecordsPos;

  if (!IsEmpty()) {
    switch (mode) {
      case Position::FIRST_POS:
        pos = 0;
        break;

      case Position::CURRENT_POS:
        pos = curr_pos_;
        break;

      case Position::LAST_POS:
        pos = records_amount_ - 1;
        break;
    }
  }

  return (pos == TArrayTable::kNoRecordsPos)
          ? std::string("") : records_[pos]->key_;
}

TDatValue* TArrayTable::GetValue(Position mode) const {
  int pos = TArrayTable::kNoRecordsPos;

  if (!IsEmpty()) {
    switch (mode) {
      case Position::FIRST_POS:
        pos = 0;
        break;

      case Position::CURRENT_POS:
        pos = curr_pos_;
        break;

      case Position::LAST_POS:
        pos = records_amount_ - 1;
        break;
    }
  }

  return (pos == TArrayTable::kNoRecordsPos) ? nullptr : records_[pos]->value_;
}

void TArrayTable::Reset() {
  curr_pos_ = 0;
}

bool TArrayTable::IsTabEnded() const {
  return curr_pos_ >= records_amount_;
}

void TArrayTable::GoNext() {
  if (!IsTabEnded())
    curr_pos_++;
}
