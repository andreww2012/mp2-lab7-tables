#include "include/TTreeNode.h"

#include <string>

TTreeNode::TTreeNode(const std::string key, TDatValue* value,
  TTreeNode* left, TTreeNode* right) :
  TTabRecord(key, value), node_left_(left), node_right_(right) {}

TTreeNode* TTreeNode::GetLeft() const {
  return node_left_;
}

TTreeNode* TTreeNode::GetRight() const {
  return node_right_;
}

TDatValue* TTreeNode::GetCopy() {
  return new TTreeNode(key_, value_, nullptr, nullptr);
}
