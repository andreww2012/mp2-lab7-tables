#include "include/TSortTable.h"

#include <string>

TSortTable::TSortTable(const int size) : TScanTable(size),
sort_method_(SortMethod::QUICK_SORT) {}

TSortTable::TSortTable(const TScanTable& table) {
  *this = table;
}

TSortTable& TSortTable::operator=(const TScanTable& table) {
  if (records_ != nullptr) {
    for (int i = 0; i < records_amount_; i++)
      delete records_[i];
    delete[] records_;
    records_ = nullptr;
  }

  size_ = table.GetTableSize();
  records_amount_ = table.GetRecordsAmount();

  for (int i = 0; i < records_amount_; i++)
    records_[i] = (TTabRecord*)table.records_[i]->GetCopy();

  SortData();
  curr_pos_ = 0;

  return *this;
}

TSortTable::SortMethod TSortTable::GetSortMethod() const {
  return sort_method_;
}

void TSortTable::SetSortMethod(SortMethod sort_method) {
  sort_method_ = sort_method;
}

TDatValue* TSortTable::FindRecord(const std::string& key) {
  // Двоичный поиск, поскольку таблица упорядоченная

  TDatValue* result = nullptr;
  if (records_amount_ > 0) {
    int i, i1 = 0, i2 = records_amount_ - 1;

    while (i1 <= i2) {
      i = (i1 + i2) / 2;
      if (records_[i]->key_ == key) {
        result = records_[i]->value_;
        break;
      }
      else if (records_[i]->key_ > key) {
        i2 = i - 1;
      }
      else {
        i1 = i + 1;
      }
    }
  }
  return result;

}

void TSortTable::InsRecord(const std::string& key, TDatValue* value) {
  if (IsFull())
    throw std::string("Table is full.");

  if (FindRecord(key) != nullptr)
    throw std::string("The record already exists.");

  for (int i = records_amount_; i > curr_pos_; i--)
    records_[i] = records_[i - 1];

  records_[curr_pos_] = new TTabRecord(key, value);
  records_amount_++;
}

void TSortTable::DelRecord(const std::string& key) {
  for (int i = 0; i < records_amount_; i++) {
    if (records_[i]->key_ == key) {
      delete records_[i];

      for (int j = i; j < records_amount_; j++) {
        records_[j] = records_[j + 1];
      }

      records_amount_--;
    }
	}
}

void TSortTable::SortData() {
  efficiency_ = 0;

  switch (sort_method_) {
    case SortMethod::INSERT_SORT:
      InsertSort(records_, records_amount_);
      break;

    case SortMethod::MERGE_SORT:
      MergeSort(records_, records_amount_);
      break;

    default:
      QuickSort(records_, records_amount_);
      break;
  }
}

void TSortTable::InsertSort(TTabRecord** records, const int size) {
  TTabRecord* rec;

  // Блок массива до индекса i уже упорядочен
  for (int i = 1, j; i < size; i++) {
    rec = records[i];
    for (j = i - 1; j > -1; j--)
      if (records[i]->key_ > rec->key_) {
        records[j + 1] = records[j];
      } else {
        break;
      }

    records[j + 1] = rec;
  }
}

void TSortTable::MergeSort(TTabRecord** records, const int size) {
  TTabRecord** data = records;
  TTabRecord** buff = new TTabRecord*[size];
  TTabRecord** temp = buff;

  MergeSorter(data, buff, size);
  if (data == temp) {  // отсортированные данные находятся в буфере
    for (int i = 0; i < size; i++) {
      buff[i] = data[i];
    }
  }

  delete temp;
}

void TSortTable::MergeSorter(TTabRecord**& data, TTabRecord**& buff,
const int size) {
  int n1 = (size + 1) / 2;
  int n2 = size - n1;

  if (size > 2) {
    TTabRecord** data2 = data + n1;
    TTabRecord** buff2 = buff + n1;

    // Сортировка "половинок" массива
    MergeSorter(data, buff, n1);
    MergeSorter(data2, buff2, n2);
  }

  // Слияние упорядоченных "половинок" массива
  MergeData(data, buff, n1, n2);
}

void TSortTable::MergeData(TTabRecord**& data, TTabRecord**& buff,
const int n1, const int n2) {
  for (int i = 0; i < (n1 + n2); i++) {
    buff[i] = data[i];
  }

  TTabRecord**& temp = data;
  data = buff;
  buff = temp;
}

void TSortTable::QuickSort(TTabRecord** records, const int size) {
  int pivot;  // индекс ведущего элемента
  int n1, n2;  // размеры разделённых блоков данных

  if (size > 1) {
    QuickSplit(records, size, pivot);  // разделение
    n1 = pivot + 1;
    n2 = size - n1;

    // Сортировка разделённых блоков массива
    QuickSort(records, n1 - 1);
    QuickSort(records + n1, n2);
  }
}

void TSortTable::QuickSplit(TTabRecord** records, const int size, int& pivot) {
  TTabRecord* pivot_pointer = records[0];  // указатель на ведущий элемента
  TTabRecord* temp;

  // Индексы левого (i1) и правого (i2) блоков
  int i1 = 1;
  int i2 = size - 1;

  while (i1 <= i2) {  // пока блоки не пересекутся
    while ((i1 < size) && !(records[i1]->key_ > pivot_pointer->key_))
      i1++;
    while (records[i2]->key_ > pivot_pointer->key_)
      i2++;

    // Перестановка элементов, на которых произошла остановка разделения
    if (i1 < i2) {
      temp = records[i1];
      records[i1] = records[i2];
      records[i2] = temp;
    }
  }

  // Установка ведущего элемента между блоками
  records[0] = records[i2];
  records[i2] = pivot_pointer;
  pivot = i2;  // i2 - индекс позиции текущего элемента
}
