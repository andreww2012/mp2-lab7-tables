#include "include/TBalanceTree.h"

#include <string>

TBalanceTree::TBalanceTree(void) : TTreeTable() {}

void TBalanceTree::InsRecord(const std::string& key, TDatValue* value) {
  if (IsFull())
    throw std::string("Table is full.");

  InsBalanceTree((TBalanceNode*&)node_root_, key, value);
}

void TBalanceTree::DelRecord(const std::string& key) {
  if (FindRecord(key) == nullptr) {
    throw std::string("Record not found.");
  }
  else {
    TTreeNode *tmp = node_root_;

    while (!iterator_stack_.empty())
      iterator_stack_.pop();
    while (tmp->GetKey() != key) {
      iterator_stack_.push(tmp);
      if (tmp->GetKey() < key)
        tmp = tmp->GetRight();
      else
        tmp = tmp->GetLeft();
    }

    std::string key2 = tmp->GetKey();
    // удаление листа
    if ((tmp->node_left_ == nullptr) && (tmp->node_right_ == nullptr)) {
      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_right_ = nullptr;
          if (prev->GetLeft() == tmp)
            prev->node_left_ = nullptr;
        }
      }
      else {
        node_root_ = nullptr;
      }
      delete tmp;
      records_amount_--;
    }
    // удаление звена с одним потомком (справа)
    else if (tmp->node_left_ == nullptr) {
      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_right_ = tmp->node_right_;
          if (prev->GetLeft() == tmp)
            prev->node_left_ = tmp->node_right_;
        }
      }
      else {
        node_root_ = tmp->GetRight();
      }
      delete tmp;
      records_amount_--;
    }
    // удаление звена с одним потомком (слева)
    else if (tmp->node_right_ == nullptr) {
      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_right_ = tmp->node_left_;
          if (prev->GetLeft() == tmp)
            prev->node_left_ = tmp->node_left_;
        }
      }
      else {
        node_root_ = tmp->GetLeft();
      }
      delete tmp;
      records_amount_--;
    }
    // удаление звена с двумя потомками
    else {
      TTreeNode *down_left = tmp->GetRight();
      while (down_left->GetLeft() != nullptr)
        down_left = down_left->node_left_;
      down_left->node_left_ = tmp->GetLeft();

      if (!iterator_stack_.empty()) {
        TTreeNode *prev = iterator_stack_.top();
        if (prev != nullptr) {
          if (prev->GetRight() == tmp)
            prev->node_right_ = tmp->node_right_;
          if (prev->GetLeft() == tmp)
            prev->node_left_ = tmp->node_right_;
        }
      }
      else {
        node_root_ = tmp->GetRight();
      }
      delete tmp;
      records_amount_--;
    }
    if (node_root_ != nullptr) {
      if (key2 < node_root_->GetKey()) {
        LeftTreeBalancing((TBalanceNode*&)node_root_);
      }
      else  if (key2 > node_root_->GetKey()) {
        RightTreeBalancing((TBalanceNode*&)node_root_);
      }
    }
  }
}

int TBalanceTree::InsBalanceTree(TBalanceNode*& node, const std::string& key,
TDatValue* value) {
  int height_index = 0;

  if (node == nullptr) {  // вставка вершины
    node = new TBalanceNode(key, value);
    height_index = 1;
    records_amount_++;
  } else if (key < node->key_) {
    // После вставки высота левого поддерева увеличилась => балансировка
    if (InsBalanceTree((TBalanceNode*&)node->node_left_, key, value) == 1)
      height_index = LeftTreeBalancing(node);
  } else if (key < node->key_) {
    if (InsBalanceTree((TBalanceNode*&)node->node_right_, key, value) == 1)
      height_index = RightTreeBalancing(node);
  } else {
    height_index = 0;
  }

  return height_index;
}

int TBalanceTree::LeftTreeBalancing(TBalanceNode*& node) {
  int height_index = 0;

  // Проверка предыдущей балансировки
  switch(node->balance_) {
    // В поддереве был перевес справа - устанавливается равновесие
    case TBalanceNode::Balance::BALANCE_RIGHT:
      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
      height_index = 0;
      break;

    // В поддереве было равновесие - устанавливается перевес слева
    case TBalanceNode::Balance::BALANCE_OK:
      node->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
      height_index = 1;
      break;

    // В поддереве был перевес слева - необходима балансировка
    case TBalanceNode::Balance::BALANCE_LEFT:
      TBalanceNode *node1, *node2;

      node1 = (TBalanceNode*)node->node_left_;

      // Случай 1 - однократный LL-поворот
      if (node1->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
        node->node_left_ = node1->node_right_;
        node1->node_right_ = node;
        node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
        node = node1;
      // Случай 2 - двукратный LR-поворот
      } else {
        node2 = (TBalanceNode*)node1->node_right_;
        node1->node_right_ = node2->node_left_;
        node2->node_left_ = node1;
        node->node_left_ = node2->node_right_;
        node2->node_right_ = node;

        if (node2->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
          node->SetBalance(TBalanceNode::Balance::BALANCE_RIGHT);
        } else if (node2->balance_ == TBalanceNode::Balance::BALANCE_RIGHT) {
          node1->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
        } else {
          node1->SetBalance(TBalanceNode::Balance::BALANCE_OK);
        }

        node = node2;
      }

      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
      height_index = 0;
      break;
  }  // switch

  return height_index;
}

int TBalanceTree::RightTreeBalancing(TBalanceNode*& node) {
  int height_index = 0;

  // Проверка предыдущей балансировки
  switch(node->balance_) {
    // В поддереве был перевес слева - устанавливается равновесие
    case TBalanceNode::Balance::BALANCE_LEFT:
      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
      height_index = 0;
      break;

    // В поддереве было равновесие - устанавливается перевес справа
    case TBalanceNode::Balance::BALANCE_OK:
      node->SetBalance(TBalanceNode::Balance::BALANCE_RIGHT);
      height_index = 1;
      break;

    // В поддереве был перевес справа - необходима балансировка
    case TBalanceNode::Balance::BALANCE_RIGHT:
      TBalanceNode *node1, *node2;

      node1 = (TBalanceNode*)node->node_right_;

      // Случай 1 - однократный RR-поворот
      if (node1->balance_ == TBalanceNode::Balance::BALANCE_RIGHT) {
        node->node_right_ = node1->node_left_;
        node1->node_left_ = node;
        node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
        node = node1;
      // Случай 2 - двукратный RL-поворот
      } else {
        node2 = (TBalanceNode*)node1->node_left_;
        node1->node_left_ = node2->node_right_;
        node2->node_right_ = node1;
        node->node_right_ = node2->node_left_;
        node2->node_left_ = node;

        if (node2->balance_ == TBalanceNode::Balance::BALANCE_RIGHT) {
          node->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
        } else if (node2->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
          node1->SetBalance(TBalanceNode::Balance::BALANCE_RIGHT);
        } else {
          node1->SetBalance(TBalanceNode::Balance::BALANCE_OK);
        }

        node = node2;
      }

      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
      height_index = 0;
      break;
  }  // switch

  return height_index;
}
