#include "include\TScanTable.h"
#include "include\TWord.h"
#include <iostream>
#include <string>

using namespace std;

int main() {
  TScanTable table(7);

  TWord* excellent = new TWord("Excellent");
  TWord* good = new TWord("Good");
  TWord* satisfactory = new TWord("Satisfactory");
  TWord* unsatisfactory = new TWord("Unsatisfactory");

  table.InsRecord("Ivanov", good);
  table.InsRecord("Kozlov", good);
  table.InsRecord("Kiselev", satisfactory);
  table.InsRecord("Zhorin", unsatisfactory);
  table.InsRecord("Sergeev", excellent);
  table.InsRecord("Saharov", unsatisfactory);
  table.InsRecord("Zheludev", satisfactory);

  cout << "Table printing" << endl;

  for (table.Reset(); !table.IsTabEnded(); table.GoNext()) {
    cout << "Key: " << table.GetKey() << ", Value: " << *(TWord*)table.GetValue() << endl;
  }

  TWord* result = (TWord*)table.FindRecord("Zhorin");

  cout << "Find Zhorin" << endl;
  cout << *result << endl;

  return 0;
}
