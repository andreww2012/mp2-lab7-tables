#Лабораторная работа №7. Таблицы

##1. Введение

Представление данных во многих задачах из разных областей человеческой деятельности может быть организовано при помощи таблиц. Таблицы представляют собой последовательности строк (записей), структура строк может быть различной, но обязательным является поле, задающее имя (ключ) записи. Таблицы применяются в бухгалтерском учете (ведомости заработной платы), в торговле (прайс-листы), в образовательных учреждениях (экзаменационные ведомости) и являются одними из наиболее распространенных структур данных, используемых при создании системного и прикладного математического обеспечения. Таблицы широко применяются в трансляторах (таблицы идентификаторов) и операционных системах, могут рассматриваться как программная реализация ассоциативной памяти и т.п. Существование отношения «иметь имя» является обязательным в большинстве разрабатываемых программистами структур данных; доступ по имени в этих структурах служит для получения соответствия между адресным принципом указания элементов памяти ЭВМ и общепринятым (более удобным для человека) способом указания объектов по их именам.

Целью лабораторной работы помимо изучения способов организации таблиц является начальное знакомство с принципами проектирования структур хранения, используемых в методах решения прикладных задач. На примере таблиц изучаются возможность выбора разных вариантов структур хранения, анализ их эффективности и определения областей приложений, в которых выбираемые структуры хранения являются наиболее эффективными.

В качестве практической задачи, на примере которой будут продемонстрированы возможные способы организации таблиц, рассматривается проблема статистической обработки результатов экзаменационной успеваемости студентов (выполнение таких, например, вычислений как определение среднего балла по предмету и/или по группе при назначении студентов на стипендию или при распределении студентов по кафедрам и т.п.).

###1.1 Основные понятия и определения

**Таблица** (от лат. tabula – доска) – динамическая структура данных, базисным множеством которой является семейство линейных структур из записей (базисное отношение включения определяется операциями вставки и удаления записей).

**Запись** – кортеж, каждый элемент которого обычно именуется полем.

**Имя записи** (ключ) – одно из полей записи, по которому обычно осуществляется поиск записей в таблице; остальные поля образуют тело записи.

**Двоичное дерево поиска** – это представление данных в виде дерева, для которого выполняются условия:

- для любого узла (вершины) дерева существует не более двух потомков (двоичное дерево);
- для любого узла значения во всех узлах левого поддерева меньше значения в узле;
- для любого узла значения во всех узлах правого поддерева больше значения в узле.

**Хеш-функция**  – функция, ставящая в соответствие ключу номер записи в таблице (используется при организации таблиц с вычислимым входом).

###1.2 Требования к лабораторной работе

В рамках данной лабораторной работы ставится задача создания программных средств, поддерживающих табличные динамические структуры данных (таблицы) и базовые операции над ними:

-	поиск записи;
-	вставка записи (без дублирования);
-	удаление записи.

Выполнение операций над таблицами может осуществляться с различной степенью эффективности в зависимости от способа организации таблицы. 

В рамках лабораторной работы как показатель эффективности предлагается использовать количество операций, необходимых для выполнения операции поиска записи в таблице. Величина этого показателя должна определяться как аналитически (при использовании тех или иных упрощающих предположений), так и экспериментально на основе проведения вычислительных экспериментов.

В лабораторной работе предлагается реализовать следующие типы таблиц:

-	просмотровые (неупорядоченные);
-	упорядоченные (сортированные);
-	таблицы со структурами хранения на основе деревьев поиска;

Необходимо разработать интерфейс доступа к операциям поиска, вставки и удаления, не зависящий от способа организации таблицы.

###1.3 Структура проекта

При выполнении данной лабораторной работы следует разработать иерархию классов, учитывая, что все таблицы имеют как общие свойства (их описание следует поместить в определении базового класса), так и особенности выполнения отдельных операций (реализуются в отдельных классах для каждого вида таблиц). При разработке классов используется ранее разработанный класс **TDatValue**.
 
Рекомендуемый состав классов приведен ниже.

**TTabRecord.h**, **TTabRecord.cpp** – модуль с классом объектов-значений для записей таблицы;

**TTable.h** – абстрактный базовый класс, содержит спецификации методов таблицы;

**TArrayTable.h**, **TArrayTable.cpp** – абстрактный базовый класс для таблиц с непрерывной памятью;

**TScanTable.h**, **TScanTable.cpp** – модуль с классом, обеспечивающим реализацию просматриваемых таблиц;

**TSortTable.h**, **TSortTable.cpp** – модуль с классом, обеспечивающим реализацию упорядоченных таблиц;

**TTreeNode.h**, **TTreeNode.cpp** – модуль с абстрактным базовым классом объектов-значений для деревьев;

**TTreeTable.h**, **TTreeTable.cpp** – модуль с классом, реализующим таблицы в виде деревьев поиска;

**TBalanceNode.h**, **TBalanceNode.cpp** – модуль с базовым классом объектов-значений для сбалансированных деревьев;

**TBalanceTree.h**, **TBalanceTree.cpp** – модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска;

##2. Выполнение лабораторной работы

###2.1 Модуль с классом объектов-значений для записей таблицы

Заголовочный файл - *TTabRecord.h*

	#pragma once

	#include "include/TDatValue.h"
	
	#include <string>
	
	// Класс объектов-значений для записей таблицы
	class TTabRecord : public TDatValue {
	 public:
	  TTabRecord(const std::string key = "", TDatValue* value = nullptr);
	  TTabRecord& operator=(TTabRecord&);
	
	  void SetKey(const std::string& key);
	  std::string GetKey(void) const;
	  void SetValue(TDatValue* value);
	  TDatValue* GetValue(void) const;
	
	  virtual TDatValue* GetCopy(void) override;
	
	  virtual bool operator==(const TTabRecord&) const;
	  virtual bool operator<(const TTabRecord&) const;
	  virtual bool operator>(const TTabRecord&) const;
	
	 protected:
	  std::string key_;  // ключ записи
	  TDatValue* value_;
	
	  // Дружественные классы для различных типов таблиц
	  friend class TArrayTable;
	  friend class TScanTable;
	  friend class TSortTable;
	  friend class TTreeNode;
	  friend class TTreeTable;
	  friend class TArrayHash;
	  friend class TListHash;
	};

Реализация - *TTabRecord.cpp*

	#include "TTabRecord.h"
	
	#include <string>
	
	TTabRecord::TTabRecord(std::string key, TDatValue* value) {
	  key_ = key;
	  value_ = value;
	}
	
	TTabRecord& TTabRecord::operator=(TTabRecord& tr) {
	  key_ = tr.key_;
	  value_ = tr.value_;
	
	  return *this;
	}
	
	void TTabRecord::SetKey(const std::string key) {
	  key_ = key;
	}
	
	std::string TTabRecord::GetKey() {
	  return key_;
	}
	
	void TTabRecord::SetValue(TDatValue* value) {
	  value_ = value;
	}
	
	TDatValue* TTabRecord::GetValue() {
	  return value_;
	}
	
	TDatValue* TTabRecord::GetCopy() {
	  TDatValue* copy = new TTabRecord(key_, value_);
	
	  return copy;
	}
	
	bool TTabRecord::operator==(const TTabRecord& tr) {
	  return key_ == tr.key_;
	}
	
	bool TTabRecord::operator<(const TTabRecord& tr) {
	  return key_ < tr.key_;
	}
	
	bool TTabRecord::operator>(const TTabRecord& tr) {
	  return key_ > tr.key_;
	}

Модульное тестирование - *test_ttabrecord.cpp*

	TEST(BakalinKazakov_TTabRecord, Can_Create_Tab_Record) {
	  ASSERT_NO_THROW(TTabRecord tab_record = TTabRecord("", nullptr));
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Assign_To_Itself) {
	  TTabRecord tab_record = TTabRecord("", nullptr);
	
	  ASSERT_NO_THROW(tab_record = tab_record);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Assign_Tab_Record) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  
	  TTabRecord tab_record2 = tab_record1;
	
	  EXPECT_EQ(tab_record1, tab_record2);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Get_Key) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	
	  EXPECT_EQ(tab_record.GetKey(), "1");
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Set_Key) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	  
	  tab_record.SetKey("2");
	
	  EXPECT_EQ(tab_record.GetKey(), "2");
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Get_Value) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	
	  EXPECT_EQ(tab_record.GetValue(), nullptr);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Set_Value) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	  
	  tab_record.SetValue(&tab_record);
	
	  EXPECT_EQ(tab_record.GetValue(), &tab_record);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Get_Copy) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  
	  TDatValue* tab_record2 = tab_record1.GetCopy();
	  tab_record1.SetKey("2");
	
	  EXPECT_EQ(((TTabRecord*)tab_record2)->GetKey(), "1");
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_Two_Equal_Records) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  TTabRecord tab_record2 = TTabRecord("1", nullptr);
	
	  EXPECT_EQ(tab_record1 == tab_record2, true);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_Two_Different_Records) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  TTabRecord tab_record2 = TTabRecord("2", nullptr);
	
	  EXPECT_EQ(tab_record1 == tab_record2, false);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_For_Less) {
	  TTabRecord tab_record1 = TTabRecord("ab", nullptr);
	  TTabRecord tab_record2 = TTabRecord("cd", nullptr);
	
	  EXPECT_EQ(tab_record1 < tab_record2, true);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_For_More) {
	  TTabRecord tab_record1 = TTabRecord("cd", nullptr);
	  TTabRecord tab_record2 = TTabRecord("ab", nullptr);
	
	  EXPECT_EQ(tab_record1 > tab_record2, true);
	}

### 2.2 Абстрактный базовый класс, содержит спецификации методов таблицы

Заголовочный файл - *TTable.h*

	#pragma once

	#include "TDatValue.h"
	
	#include <string>
	#include <iostream>
	
	// Абстрактный базовый класс, содержащий спецификацию методов таблицы
	class TTable {
	 public:
	  enum Position {
	    FIRST_POS,
	    CURRENT_POS,
	    LAST_POS
	  };
	
	  TTable(void) : entries_amount_(0), efficiency_(0) {}
	  virtual ~TTable(void) {}
	
	  int GetEntriesAmount(void) const { return entries_amount_; }
	  int GetEfficiency(void) const { return efficiency_; }
	
	  bool IsEmpty(void) const { return entries_amount_ == 0; }
	  virtual bool IsFull(void) const = 0;
	
	  virtual std::string GetKey(Position mode = Position::CURRENT_POS) const = 0;
	  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS) const = 0;
	
	  virtual TDatValue* FindRecord(std::string key) = 0;
	  virtual void InsRecord(std::string key, TDatValue* value) = 0;
	  virtual void DelRecord(std::string key) = 0;
	
	  virtual void Reset(void) = 0;
	  virtual bool IsTabEnded(void) const = 0;
	  virtual void GoNext(void) = 0;
	
	 protected:
	  int entries_amount_;  // количество записей, но не размер таблицы
	  int efficiency_;  // показатель эффективности выполнения операции
	};

### 2.3 Абстрактный базовый класс для таблиц с непрерывной памятью

Заголовочный файл - *TArrayTable.h*

	#pragma once

	#include "include/TTable.h"
	#include "include/TTabRecord.h"
	
	#include <string>
	
	// Абстрактный класс для таблиц с непрерывной памятью
	// для управления структурой хранения
	class TArrayTable : public TTable {
	 public:
	  static const int kTabDefaultSize = 25;
	  static const int kNoRecordsPos = -1;
	
	  explicit TArrayTable(const int size = kTabDefaultSize);
	  ~TArrayTable(void);
	
	  virtual bool IsFull(void) const override;
	  int GetTableSize(void) const;
	
	  int GetCurrentPos(void) const;
	  virtual void SetCurrentPos(const int pos);
	
	  virtual std::string GetKey(Position mode = Position::CURRENT_POS)
	  const override;
	  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS)
	  const override;
	
	  virtual void Reset(void) override;
	  virtual bool IsTabEnded(void) const override;
	  virtual void GoNext(void) override;
	
	 protected:
	  TTabRecord** records_;
	  int size_;
	  int curr_pos_;
	
	  friend TSortTable;
	};


Реализация - *TArrayTable.cpp*

	#include "include/TArrayTable.h"

	#include <string>
	
	TArrayTable::TArrayTable(const int size) {
	  if (size <= 0)
	    throw std::string("Incorrect size.");
	
	  records_ = new TTabRecord*[size];
	  for (int i = 0; i < size; i++) {
	    records_[i] = nullptr;
	  }
	
	  size_ = size;
	  curr_pos_ = 0;
	}
	
	TArrayTable::~TArrayTable() {
	  for (int i = 0; i < size_; i++) {
	    if (records_[i] != nullptr)
	      delete records_[i];
	  }
	
	  delete[] records_;
	}
	
	bool TArrayTable::IsFull() const {
	  return records_amount_ >= size_;
	}
	
	int TArrayTable::GetTableSize() const {
	  return size_;
	}
	
	int TArrayTable::GetCurrentPos() const {
	  return curr_pos_;
	}
	
	void TArrayTable::SetCurrentPos(const int pos) {
	  curr_pos_ = ((pos > TArrayTable::kNoRecordsPos)
	                && (pos < records_amount_)) ? pos : 0;
	}
	
	std::string TArrayTable::GetKey(Position mode) const {
	  int pos = TArrayTable::kNoRecordsPos;
	
	  if (!IsEmpty()) {
	    switch (mode) {
	      case Position::FIRST_POS:
	        pos = 0;
	        break;
	
	      case Position::CURRENT_POS:
	        pos = curr_pos_;
	        break;
	
	      case Position::LAST_POS:
	        pos = records_amount_ - 1;
	        break;
	    }
	  }
	
	  return (pos == TArrayTable::kNoRecordsPos)
	          ? std::string("") : records_[pos]->key_;
	}
	
	TDatValue* TArrayTable::GetValue(Position mode) const {
	  int pos = TArrayTable::kNoRecordsPos;
	
	  if (!IsEmpty()) {
	    switch (mode) {
	      case Position::FIRST_POS:
	        pos = 0;
	        break;
	
	      case Position::CURRENT_POS:
	        pos = curr_pos_;
	        break;
	
	      case Position::LAST_POS:
	        pos = records_amount_ - 1;
	        break;
	    }
	  }
	
	  return (pos == TArrayTable::kNoRecordsPos) ? nullptr : records_[pos]->value_;
	}
	
	void TArrayTable::Reset() {
	  curr_pos_ = 0;
	}
	
	bool TArrayTable::IsTabEnded() const {
	  return curr_pos_ >= records_amount_;
	}
	
	void TArrayTable::GoNext() {
	  if (!IsTabEnded())
	    curr_pos_++;
	}

###2.4 Модуль с классом, обеспечивающим реализацию просматриваемых таблиц

Заголовочный файл - *TScanTable.h*

	#pragma once
	
	#include "TArrayTable.h"
	
	#include <string>
	
	// Класс просматриваемых таблиц
	class TScanTable : public TArrayTable {
	 public:
	  TScanTable(int size = TArrayTable::kTabDefaultSize);
	
	  virtual TDatValue* FindRecord(const std::string& key) override;
	  virtual void InsRecord(const std::string& key, TDatValue* value) override;
	  virtual void DelRecord(const std::string& key) override;
	};

Реализация - *TScanTable.cpp*

	#include "include/TScanTable.h"

	#include <string>
	
	TScanTable::TScanTable(const int size) : TArrayTable(size) {}
	
	TDatValue* TScanTable::FindRecord(const std::string& key) {
	  int i;
	
	  for (i = 0; i < records_amount_; i++) {
	    if (records_[i]->key_ == key)
	      break;
	  }
	
	  if (i < records_amount_) {
	    curr_pos_ = i;
	    return records_[i]->value_;
	  }
	
	  return nullptr;
	}
	
	void TScanTable::InsRecord(const std::string& key, TDatValue* value) {
	  for (int i = 0; i < size_; i++) {
	    if (records_[i] == nullptr) {
	      records_[i] = new TTabRecord(key, value);
	      records_amount_++;
	      break;
	    }
	  }
	}
	
	void TScanTable::DelRecord(const std::string& key) {
	  for (int i = 0; i < size_; i++) {
	    if (records_[i] != nullptr)
	      if (records_[i]->GetKey() == key) {
	        delete records_[i];
	        records_[i] = records_[records_amount_ - 1];
	        records_[records_amount_ - 1] = nullptr;
	        records_amount_--;
	        break;
	      }
	  }
	}

Модульное тестирование - *test_tscantable.cpp*

	#include <gtest/gtest.h>

	#include "include/TScanTable.h"
	#include "include/TWord.h"
	
	TEST(BakalinKazakov_TScanTable, Can_Create_Scan_Table) {
	  ASSERT_NO_THROW(TScanTable scan_table());
	}
	
	TEST(BakalinKazakov_TScanTable, Cant_Create_Scan_Table_With_Negative_Size) {
	  ASSERT_ANY_THROW(TScanTable scan_table(-14));
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Insert_Record) {
	  TScanTable scan_table(5);
	  TWord *word = new TWord("Multiprogramm");
	
	  ASSERT_NO_THROW(scan_table.InsRecord("ab", word));
	}
	
	TEST(BakalinKazakov_TScanTable, IsFull_Works_Correctly) {
	  TScanTable scan_table(3);
	  TWord *word = new TWord("Multiprogramm");
	
	  scan_table.InsRecord("aa", word);
	  scan_table.InsRecord("ab", word);
	  scan_table.InsRecord("ac", word);
	
	  EXPECT_EQ(scan_table.IsFull(), true);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Get_Table_Size) {
	  TScanTable scan_table(3);
	
	  EXPECT_EQ(scan_table.GetTableSize(), 3);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Get_Current_Position) {
	  TScanTable scan_table(3);
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Set_Current_Position) {
	  TScanTable scan_table(3);
	  TWord *word = new TWord("Multiprogramm");
	  scan_table.InsRecord("aa", word);
	  scan_table.InsRecord("ab", word);
	
	  scan_table.SetCurrentPos(1);
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 1);
	}
	
	TEST(BakalinKazakov_TScanTable, Cant_Set_Current_Position_If_Tab_Is_Empty){
	  TScanTable scan_table(3);
	
	  scan_table.SetCurrentPos(1);
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Go_Next) {
	  TScanTable scan_table(3);
	  TWord *word = new TWord("Multiprogramm");
	  scan_table.InsRecord("aa", word);
	
	  scan_table.Reset();
	  scan_table.GoNext();
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 1);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Reset_Table) {
	  TScanTable scan_table(3);
	
	  scan_table.GoNext();
	  scan_table.Reset();
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TScanTable, IsTabEnded_Works_Correctly) {
	  TScanTable scan_table(3);
	
	  scan_table.GoNext();
	  scan_table.GoNext();
	  scan_table.GoNext();
	
	  EXPECT_EQ(scan_table.IsTabEnded(), true);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Find_Record) {
	  TScanTable scan_table(3);
	  TWord *word1 = new TWord("Multiprogramm");
	  TWord *word2 = new TWord("SGBash");
	  scan_table.InsRecord("aa", word1);
	  scan_table.InsRecord("ab", word2);
	
	  TWord* result = (TWord*)scan_table.FindRecord("ab");
	
	  EXPECT_EQ(result->GetWord(), "SGBash");
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Delete_Record) {
	  TScanTable scan_table(3);
	  TWord *word1 = new TWord("Multiprogramm");
	  TWord *word2 = new TWord("SGBash");
	  scan_table.InsRecord("aa", word1);
	  scan_table.InsRecord("ab", word2);
	
	  scan_table.DelRecord("ab");
	  TWord* result = (TWord*)scan_table.FindRecord("ab");
	
	  EXPECT_EQ(result, nullptr);
	}

###2.5 Модуль с классом, обеспечивающим реализацию упорядоченных таблиц

Заголовочный файл - *TSortTable.h*

	#pragma once
	
	#include "include/TScanTable.h"
	
	#include <string>
	
	// Класс упорядоченных таблиц
	class TSortTable: public TScanTable {
	 public:
	  enum SortMethod {
	    INSERT_SORT,
	    MERGE_SORT,
	    QUICK_SORT
	  };
	
	  explicit TSortTable(const int size = kTabDefaultSize);
	  TSortTable(const TScanTable&);
	  TSortTable& operator=(const TScanTable&);
	
	  SortMethod GetSortMethod(void) const;
	  void SetSortMethod(SortMethod sort_method);
	
	  virtual TDatValue* FindRecord(const std::string& key) override;
	  virtual void InsRecord(const std::string& key, TDatValue* value) override;
	  virtual void DelRecord(const std::string& key) override;
	
	 protected:
	  SortMethod sort_method_;
	
	  void SortData(void);
	
	  static void InsertSort(TTabRecord** records, const int size);
	
	  static void MergeSort(TTabRecord** records, const int size);
	  static void MergeSorter(TTabRecord**& data, TTabRecord**& buff,
	  const int size);
	  static void MergeData(TTabRecord**& data, TTabRecord**& buff,
	  const int n1, const int n2);
	
	  // Быстрая сортировка
	  static void QuickSort(TTabRecord** records, const int size);
	  // Быстрая сортировка - выбор ведущего элемента и разделение данных
	  static void QuickSplit(TTabRecord** records, const int size, int& pivot);
	};

Реализация - *TSortTable.cpp*

	#include "include/TSortTable.h"
	
	#include <string>
	
	TSortTable::TSortTable(const int size) : TScanTable(size),
	sort_method_(SortMethod::QUICK_SORT) {}
	
	TSortTable::TSortTable(const TScanTable& table) {
	  *this = table;
	}
	
	TSortTable& TSortTable::operator=(const TScanTable& table) {
	  if (records_ != nullptr) {
	    for (int i = 0; i < records_amount_; i++)
	      delete records_[i];
	    delete[] records_;
	    records_ = nullptr;
	  }
	
	  size_ = table.GetTableSize();
	  records_amount_ = table.GetRecordsAmount();
	
	  for (int i = 0; i < records_amount_; i++)
	    records_[i] = (TTabRecord*)table.records_[i]->GetCopy();
	
	  SortData();
	  curr_pos_ = 0;
	
	  return *this;
	}
	
	TSortTable::SortMethod TSortTable::GetSortMethod() const {
	  return sort_method_;
	}
	
	void TSortTable::SetSortMethod(SortMethod sort_method) {
	  sort_method_ = sort_method;
	}
	
	TDatValue* TSortTable::FindRecord(const std::string& key) {
	  // Двоичный поиск, поскольку таблица упорядоченная
	
	  TDatValue* result = nullptr;
	  if (records_amount_ > 0) {
	    int i, i1 = 0, i2 = records_amount_ - 1;
	
	    while (i1 <= i2) {
	      i = (i1 + i2) / 2;
	      if (records_[i]->key_ == key) {
	        result = records_[i]->value_;
	        break;
	      }
	      else if (records_[i]->key_ > key) {
	        i2 = i - 1;
	      }
	      else {
	        i1 = i + 1;
	      }
	    }
	  }
	  return result;
	
	}
	
	void TSortTable::InsRecord(const std::string& key, TDatValue* value) {
	  if (IsFull())
	    throw std::string("Table is full.");
	
	  if (FindRecord(key) != nullptr)
	    throw std::string("The record already exists.");
	
	  for (int i = records_amount_; i > curr_pos_; i--)
	    records_[i] = records_[i - 1];
	
	  records_[curr_pos_] = new TTabRecord(key, value);
	  records_amount_++;
	}
	
	void TSortTable::DelRecord(const std::string& key) {
	  for (int i = 0; i < records_amount_; i++) {
	    if (records_[i]->key_ == key) {
	      delete records_[i];
	
	      for (int j = i; j < records_amount_; j++) {
	        records_[j] = records_[j + 1];
	      }
	
	      records_amount_--;
	    }
		}
	}
	
	void TSortTable::SortData() {
	  efficiency_ = 0;
	
	  switch (sort_method_) {
	    case SortMethod::INSERT_SORT:
	      InsertSort(records_, records_amount_);
	      break;
	
	    case SortMethod::MERGE_SORT:
	      MergeSort(records_, records_amount_);
	      break;
	
	    default:
	      QuickSort(records_, records_amount_);
	      break;
	  }
	}
	
	void TSortTable::InsertSort(TTabRecord** records, const int size) {
	  TTabRecord* rec;
	
	  // Блок массива до индекса i уже упорядочен
	  for (int i = 1, j; i < size; i++) {
	    rec = records[i];
	    for (j = i - 1; j > -1; j--)
	      if (records[i]->key_ > rec->key_) {
	        records[j + 1] = records[j];
	      } else {
	        break;
	      }
	
	    records[j + 1] = rec;
	  }
	}
	
	void TSortTable::MergeSort(TTabRecord** records, const int size) {
	  TTabRecord** data = records;
	  TTabRecord** buff = new TTabRecord*[size];
	  TTabRecord** temp = buff;
	
	  MergeSorter(data, buff, size);
	  if (data == temp) {  // отсортированные данные находятся в буфере
	    for (int i = 0; i < size; i++) {
	      buff[i] = data[i];
	    }
	  }
	
	  delete temp;
	}
	
	void TSortTable::MergeSorter(TTabRecord**& data, TTabRecord**& buff,
	const int size) {
	  int n1 = (size + 1) / 2;
	  int n2 = size - n1;
	
	  if (size > 2) {
	    TTabRecord** data2 = data + n1;
	    TTabRecord** buff2 = buff + n1;
	
	    // Сортировка "половинок" массива
	    MergeSorter(data, buff, n1);
	    MergeSorter(data2, buff2, n2);
	  }
	
	  // Слияние упорядоченных "половинок" массива
	  MergeData(data, buff, n1, n2);
	}
	
	void TSortTable::MergeData(TTabRecord**& data, TTabRecord**& buff,
	const int n1, const int n2) {
	  for (int i = 0; i < (n1 + n2); i++) {
	    buff[i] = data[i];
	  }
	
	  TTabRecord**& temp = data;
	  data = buff;
	  buff = temp;
	}
	
	void TSortTable::QuickSort(TTabRecord** records, const int size) {
	  int pivot;  // индекс ведущего элемента
	  int n1, n2;  // размеры разделённых блоков данных
	
	  if (size > 1) {
	    QuickSplit(records, size, pivot);  // разделение
	    n1 = pivot + 1;
	    n2 = size - n1;
	
	    // Сортировка разделённых блоков массива
	    QuickSort(records, n1 - 1);
	    QuickSort(records + n1, n2);
	  }
	}
	
	void TSortTable::QuickSplit(TTabRecord** records, const int size, int& pivot) {
	  TTabRecord* pivot_pointer = records[0];  // указатель на ведущий элемента
	  TTabRecord* temp;
	
	  // Индексы левого (i1) и правого (i2) блоков
	  int i1 = 1;
	  int i2 = size - 1;
	
	  while (i1 <= i2) {  // пока блоки не пересекутся
	    while ((i1 < size) && !(records[i1]->key_ > pivot_pointer->key_))
	      i1++;
	    while (records[i2]->key_ > pivot_pointer->key_)
	      i2++;
	
	    // Перестановка элементов, на которых произошла остановка разделения
	    if (i1 < i2) {
	      temp = records[i1];
	      records[i1] = records[i2];
	      records[i2] = temp;
	    }
	  }
	
	  // Установка ведущего элемента между блоками
	  records[0] = records[i2];
	  records[i2] = pivot_pointer;
	  pivot = i2;  // i2 - индекс позиции текущего элемента
	}

Модульное тестирование - *test_tsorttable.cpp*

	#include <gtest/gtest.h>
	
	#include "include/TSortTable.h"
	#include "include/TWord.h"
	
	TEST(BakalinKazakov_TSortTable, Can_Create_Sort_Table) {
	  ASSERT_NO_THROW(TSortTable table());
	}
	
	TEST(BakalinKazakov_TSortTable, Cant_Create_Sort_Table_With_Negative_Size) {
	  ASSERT_ANY_THROW(TSortTable table(-14));
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Set_Sort_Method) {
	  TSortTable table(5);
	
	  ASSERT_NO_THROW(table.SetSortMethod(TSortTable::SortMethod::INSERT_SORT));
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Get_Sort_Method) {
	  TSortTable table(5);
	
	  table.SetSortMethod(TSortTable::SortMethod::INSERT_SORT);
	
	  ASSERT_NO_THROW(table.GetSortMethod());
	}
	
	TEST(BakalinKazakov_TSortTable, Get_Sort_Method_Works_Properly) {
	  TSortTable table(5);
	
	  table.SetSortMethod(TSortTable::SortMethod::INSERT_SORT);
	
	  EXPECT_EQ(table.GetSortMethod(), TSortTable::SortMethod::INSERT_SORT);
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Insert_Record) {
	  TSortTable table(5);
	  TWord* word = new TWord("Multiprogramm");
	
	  ASSERT_NO_THROW(table.InsRecord("ab", word));
	}
	
	TEST(BakalinKazakov_TSortTable, IsFull_Works_Correctly) {
	  TSortTable table(3);
	  TWord* word = new TWord("Multiprogramm");
	
	  table.InsRecord("aa", word);
	  table.InsRecord("ab", word);
	  table.InsRecord("ac", word);
	
	  EXPECT_EQ(table.IsFull(), true);
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Get_Table_Size) {
	  TSortTable table(3);
	
	  EXPECT_EQ(table.GetTableSize(), 3);
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Get_Current_Position) {
	  TSortTable table(3);
	
	  EXPECT_EQ(table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Set_Current_Position) {
	  TSortTable table(3);
	  TWord* word = new TWord("Multiprogramm");
	  table.InsRecord("aa", word);
	  table.InsRecord("ab", word);
	
	  table.SetCurrentPos(1);
	
	  EXPECT_EQ(table.GetCurrentPos(), 1);
	}
	
	TEST(BakalinKazakov_TSortTable, Cant_Set_Current_Position_If_Tab_Is_Empty){
	  TSortTable table(3);
	
	  table.SetCurrentPos(1);
	
	  EXPECT_EQ(table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Go_Next) {
	  TSortTable table(3);
	  TWord* word = new TWord("Multiprogramm");
	  table.InsRecord("aa", word);
	
	  table.Reset();
	  table.GoNext();
	
	  EXPECT_EQ(table.GetCurrentPos(), 1);
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Reset_Table) {
	  TSortTable table(3);
	
	  table.GoNext();
	  table.Reset();
	
	  EXPECT_EQ(table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TSortTable, IsTabEnded_Works_Correctly) {
	  TSortTable table(3);
	
	  table.GoNext();
	  table.GoNext();
	  table.GoNext();
	
	  EXPECT_EQ(table.IsTabEnded(), true);
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Find_Record) {
	  TSortTable table(3);
	  TWord* word1 = new TWord("Multiprogramm");
	  TWord* word2 = new TWord("SGBash");
	  table.InsRecord("aa", word1);
	  table.InsRecord("ab", word2);
	
	  TWord* result = (TWord*)table.FindRecord("ab");
	
	  EXPECT_EQ(result->GetWord(), "SGBash");
	}
	
	TEST(BakalinKazakov_TSortTable, Can_Delete_Record) {
	  TSortTable table(3);
	  TWord* word1 = new TWord("Multiprogramm");
	  TWord* word2 = new TWord("SGBash");
	  table.InsRecord("aa", word1);
	  table.InsRecord("ab", word2);
	
	  table.DelRecord("ab");
	  TWord* result = (TWord*)table.FindRecord("ab");
	
	  EXPECT_EQ(result, nullptr);
	}

###2.6 Модуль с абстрактным базовым классом объектов-значений для деревьев

Заголовочный файл - *TTreeNode.h*

	#pragma once

	#include "include/TTabRecord.h"
	
	#include <string>
	
	// Абстрактный базовый класс объектов-значений для деревьев
	class TTreeNode : public TTabRecord {
	 public:
	  TTreeNode(const std::string key = "", TDatValue* value = nullptr,
	  TTreeNode* left = nullptr, TTreeNode* right = nullptr);
	
	  TTreeNode* GetLeft(void) const;
	  TTreeNode* GetRight(void) const;
	
	  virtual TDatValue* GetCopy(void) override;
	
	 protected:
	  TTreeNode *node_left_, *node_right_;
	
	  friend class TTreeTable;
	  friend class TBalanceTree;
	};

Реализация - *TTreeNode.cpp*

	#include "include/TTreeNode.h"

	#include <string>
	
	TTreeNode::TTreeNode(const std::string key, TDatValue* value,
	  TTreeNode* left, TTreeNode* right) :
	  TTabRecord(key, value), node_left_(left), node_right_(right) {}
	
	TTreeNode* TTreeNode::GetLeft() const {
	  return node_left_;
	}
	
	TTreeNode* TTreeNode::GetRight() const {
	  return node_right_;
	}
	
	TDatValue* TTreeNode::GetCopy() {
	  return new TTreeNode(key_, value_, nullptr, nullptr);
	}

###2.7 Модуль с классом, реализующим таблицы в виде деревьев поиска

Заголовочный файл - *TTreeTable.h*

	#pragma once
	
	#include "include/TTreeNode.h"
	#include "include/TTable.h"
	
	#include <stack>
	#include <string>
	
	// Таблицы со структурой хранения в виде деревьев поиска
	class TTreeTable: public TTable {
	 public:
	  TTreeTable(void);
	  ~TTreeTable(void);
	
	  virtual bool IsFull(void) const override;
	
	  virtual std::string GetKey(Position mode = Position::CURRENT_POS) const override;
	  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS) const override;
	
	  virtual TDatValue* FindRecord(const std::string& key) override;
	  virtual void InsRecord(const std::string& key, TDatValue* value) override;
	  virtual void DelRecord(const std::string& key) override;
	
	  virtual void Reset(void) override;
	  virtual bool IsTabEnded(void) const override;
	  virtual void GoNext(void) override;
	
	 protected:
	  TTreeNode *node_root_;
	  TTreeNode *node_current_;
	
	  // Адрес указателя на вершину результата в FindRecord
	  TTreeNode **node_found_;
	
	  int curr_pos_;  // номер текущей вершины
	  std::stack<TTreeNode*> iterator_stack_;  // стек для итератора
	
	  static void DeleteTreeTable(TTreeNode*);
	};

Реализация - *TTreeTable.cpp*

	#include "include/TTreeTable.h"
	
	#include <stack>
	#include <string>
	
	TTreeTable::TTreeTable(): TTable() {
	  curr_pos_ = 0;
	  node_root_ = node_current_ = nullptr;
	  node_found_ = nullptr;
	}
	
	TTreeTable::~TTreeTable() {
	  DeleteTreeTable(node_root_);
	}
	
	bool TTreeTable::IsFull() const {
	  TTreeNode* node = new TTreeNode();
	  bool result = (node == nullptr);
	  delete node;
	
	  return result;
	}
	
	std::string TTreeTable::GetKey(Position mode) const {
	  return (node_current_ == nullptr) ? std::string("") : node_current_->key_;
	}
	
	TDatValue* TTreeTable::GetValue(Position mode) const {
	  return (node_current_ == nullptr) ? nullptr : node_current_->value_;
	}
	
	TDatValue* TTreeTable::FindRecord(const std::string& key) {
	  TTreeNode* node = node_root_;
	  node_found_ = &node_root_;
	
	  while (node != nullptr) {
	    if (node->key_ == key) break;
	
	    if (node->key_ < key) {
	      node_found_ = &node->node_right_;
	    } else {
	      node_found_ = &node->node_left_;
	    }
	
	    node = *node_found_;
	  }
	
	  return (node == nullptr) ? nullptr : node->value_;
	}
	
	void TTreeTable::InsRecord(const std::string& key, TDatValue* value) {
	  if (IsFull())
	    throw std::string("Table is full.");
	
	  if (FindRecord(key) != nullptr)
	    throw std::string("The record already exists.");
	
	  *node_found_ = new TTreeNode(key, value);
	  records_amount_++;
	}
	
	void TTreeTable::DelRecord(const std::string& key) {
	  if (FindRecord(key) == nullptr) {
	    throw std::string("Record not found.");
	  }
	  else {
	    TTreeNode *tmp = node_root_;
	
	    while (!iterator_stack_.empty())
	      iterator_stack_.pop();
	
	    while (tmp->GetKey() != key) {
	      iterator_stack_.push(tmp);
	
	      if (tmp->GetKey() < key)
	        tmp = tmp->GetRight();
	      else
	        tmp = tmp->GetLeft();
	    }
	
	    if ((tmp->node_left_ == nullptr) && (tmp->node_right_ == nullptr)) {
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_right_ = nullptr;
	          else if (prev->GetLeft() == tmp)
	            prev->node_left_ = nullptr;
	        }
	      }
	      else {
	        node_root_ = nullptr;
	      }
	      delete tmp;
	      records_amount_--;
	    }
	    else if (tmp->node_left_ == nullptr) {
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_left_ = tmp->node_right_;
	          if (prev->GetLeft() == tmp)
	            prev->node_left_ = tmp->node_right_;
	        }
	      }
	      else {
	        node_root_ = tmp->GetRight();
	      }
	      delete tmp;
	      records_amount_--;
	    }
	    else if (tmp->node_right_ == nullptr) {
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_right_ = tmp->node_left_;
	          if (prev->GetLeft() == tmp)
	            prev->node_left_ = tmp->node_left_;
	        }
	      }
	      else {
	        node_root_ = tmp->GetLeft();
	      }
	      delete tmp;
	      records_amount_--;
	    }
	    else {
	      TTreeNode *down_left = tmp->GetRight();
	      while (down_left->GetLeft() != nullptr)
	        down_left = down_left->node_left_;
	      down_left->node_left_ = tmp->GetLeft();
	
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_right_ = tmp->node_right_;
	          if (prev->GetLeft() == tmp)
	            prev->node_left_ = tmp->node_right_;
	        }
	      }
	      else {
	        node_root_ = tmp->GetRight();
	      }
	      delete tmp;
	      records_amount_--;
	    }
	  }
	}
	
	void TTreeTable::Reset() {
	  TTreeNode* node = node_current_ = node_root_;
	
	  while (!iterator_stack_.empty())
	    iterator_stack_.pop();
	
	  curr_pos_ = 0;
	
	  while (node != nullptr) {
	    iterator_stack_.push(node);
	    node_current_ = node;
	    node = node->node_left_;
	  }
	}
	
	bool TTreeTable::IsTabEnded() const {
	  return curr_pos_ >= records_amount_;
	}
	
	void TTreeTable::GoNext() {
	  records_amount_++;
	
	  if (!IsTabEnded() && (node_current_ != nullptr)) {
	    TTreeNode *node = node_current_ = node_current_->GetRight();
	    iterator_stack_.pop();
	
	    while (node != nullptr) {
	      iterator_stack_.push(node);
	      node_current_ = node;
	      node = node->GetLeft();
	    }
	
	    if ((node_current_ == nullptr) && !iterator_stack_.empty())
	      node_current_ = iterator_stack_.top();
	  }
	}
	
	void TTreeTable::DeleteTreeTable(TTreeNode* node) {
	  if (node != nullptr) {
	    DeleteTreeTable(node->node_left_);
	    DeleteTreeTable(node->node_right_);
	    delete node;
	  }
	}

Модульное тестирование - *test_ttreetable.h*

	#include <gtest/gtest.h>
	
	#include "include/TTreeTable.h"
	#include "include/TWord.h"
	
	TEST(BakalinKazakov_TTreeTable, Can_Insert_Record) {
	  TTreeTable table;
	  TWord* word = new TWord("Multiprogramm");
	
	  ASSERT_NO_THROW(table.InsRecord("ab", word));
	}
	
	TEST(BakalinKazakov_TTreeTable, Can_Find_Record) {
	  TTreeTable table;
	  TWord* word1 = new TWord("Multiprogramm");
	  TWord* word2 = new TWord("SGBash");
	  table.InsRecord("aa", word1);
	  table.InsRecord("ab", word2);
	
	  TWord* result = (TWord*)table.FindRecord("ab");
	
	  EXPECT_EQ(result->GetWord(), "SGBash");
	}
	
	TEST(BakalinKazakov_TTreeTable, Can_Delete_Record) {
	  TTreeTable table;
	  TWord* word1 = new TWord("Multiprogramm");
	  TWord* word2 = new TWord("SGBash");
	  table.InsRecord("aa", word1);
	  table.InsRecord("ab", word2);
	
	  table.DelRecord("ab");
	  TWord* result = (TWord*)table.FindRecord("ab");
	
	  EXPECT_EQ(result, nullptr);
	}

###2.8 Модуль с базовым классом объектов-значений для сбалансированных деревьев

Заголовочный файл - *TBalanceNode.h*

	#pragma once
	
	#include "include/TTreeNode.h"
	
	#include <string>
	
	// Базовый класс объектов-значений для сбалансированных деревьев
	class TBalanceNode : public TTreeNode {
	 public:
	  enum Balance {
	    BALANCE_OK,
	    BALANCE_LEFT,
	    BALANCE_RIGHT
	  };
	
	  TBalanceNode(const std::string key = "", TDatValue* value = nullptr,
	  TTreeNode* left = nullptr, TTreeNode* right = nullptr,
	  Balance balance = Balance::BALANCE_OK);
	
	  Balance GetBalance(void) const;
	  void SetBalance(Balance balance);
	
	  virtual TDatValue* GetCopy(void) override;
	
	 protected:
	  Balance balance_;  // индекс балансировки вершины
	
	  friend class TBalanceTree;
	};

Реализация - *TBalanceNode.cpp*

	#include "include/TBalanceNode.h"
	
	#include <string>
	
	TBalanceNode::TBalanceNode(const std::string key, TDatValue* value,
	  TTreeNode* left, TTreeNode* right, Balance balance) :
	  TTreeNode(key, value, left, right), balance_(balance) {}
	
	TBalanceNode::Balance TBalanceNode::GetBalance() const {
	  return balance_;
	}
	
	void TBalanceNode::SetBalance(TBalanceNode::Balance balance) {
	  balance_ = balance;
	}
	
	TDatValue* TBalanceNode::GetCopy() {
	  return new TBalanceNode(key_, value_, nullptr, nullptr, Balance::BALANCE_OK);
	}

###2.9 Модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска

Заголовочный файл - *TBalanceTree.h*

	#pragma once
	
	#include "include/TTreeTable.h"
	#include "include/TBalanceNode.h"
	
	#include <string>
	
	// Класс для сбалансированных деревьев (TBalanceTree).
	class TBalanceTree : public TTreeTable  {
	 public:
	  TBalanceTree(void);
	
	  virtual void InsRecord(const std::string& key, TDatValue* value) override;
	  virtual void DelRecord(const std::string& key) override;
	
	 protected:
	  int InsBalanceTree(TBalanceNode*& node, const std::string& key,
	  TDatValue* value);
	
	  int LeftTreeBalancing(TBalanceNode*&);
	  int RightTreeBalancing(TBalanceNode*&);
	};

Реализация - *TBalanceTree.cpp*

	#include "include/TBalanceTree.h"
	
	#include <string>
	
	TBalanceTree::TBalanceTree(void) : TTreeTable() {}
	
	void TBalanceTree::InsRecord(const std::string& key, TDatValue* value) {
	  if (IsFull())
	    throw std::string("Table is full.");
	
	  InsBalanceTree((TBalanceNode*&)node_root_, key, value);
	}
	
	void TBalanceTree::DelRecord(const std::string& key) {
	  if (FindRecord(key) == nullptr) {
	    throw std::string("Record not found.");
	  }
	  else {
	    TTreeNode *tmp = node_root_;
	
	    while (!iterator_stack_.empty())
	      iterator_stack_.pop();
	    while (tmp->GetKey() != key) {
	      iterator_stack_.push(tmp);
	      if (tmp->GetKey() < key)
	        tmp = tmp->GetRight();
	      else
	        tmp = tmp->GetLeft();
	    }
	
	    std::string key2 = tmp->GetKey();
	    // удаление листа
	    if ((tmp->node_left_ == nullptr) && (tmp->node_right_ == nullptr)) {
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_right_ = nullptr;
	          if (prev->GetLeft() == tmp)
	            prev->node_left_ = nullptr;
	        }
	      }
	      else {
	        node_root_ = nullptr;
	      }
	      delete tmp;
	      records_amount_--;
	    }
	    // удаление звена с одним потомком (справа)
	    else if (tmp->node_left_ == nullptr) {
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_right_ = tmp->node_right_;
	          if (prev->GetLeft() == tmp)
	            prev->node_left_ = tmp->node_right_;
	        }
	      }
	      else {
	        node_root_ = tmp->GetRight();
	      }
	      delete tmp;
	      records_amount_--;
	    }
	    // удаление звена с одним потомком (слева)
	    else if (tmp->node_right_ == nullptr) {
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_right_ = tmp->node_left_;
	          if (prev->GetLeft() == tmp)
	            prev->node_left_ = tmp->node_left_;
	        }
	      }
	      else {
	        node_root_ = tmp->GetLeft();
	      }
	      delete tmp;
	      records_amount_--;
	    }
	    // удаление звена с двумя потомками
	    else {
	      TTreeNode *down_left = tmp->GetRight();
	      while (down_left->GetLeft() != nullptr)
	        down_left = down_left->node_left_;
	      down_left->node_left_ = tmp->GetLeft();
	
	      if (!iterator_stack_.empty()) {
	        TTreeNode *prev = iterator_stack_.top();
	        if (prev != nullptr) {
	          if (prev->GetRight() == tmp)
	            prev->node_right_ = tmp->node_right_;
	          if (prev->GetLeft() == tmp)
	            prev->node_left_ = tmp->node_right_;
	        }
	      }
	      else {
	        node_root_ = tmp->GetRight();
	      }
	      delete tmp;
	      records_amount_--;
	    }
	    if (node_root_ != nullptr) {
	      if (key2 < node_root_->GetKey()) {
	        LeftTreeBalancing((TBalanceNode*&)node_root_);
	      }
	      else  if (key2 > node_root_->GetKey()) {
	        RightTreeBalancing((TBalanceNode*&)node_root_);
	      }
	    }
	  }
	}
	
	int TBalanceTree::InsBalanceTree(TBalanceNode*& node, const std::string& key,
	TDatValue* value) {
	  int height_index = 0;
	
	  if (node == nullptr) {  // вставка вершины
	    node = new TBalanceNode(key, value);
	    height_index = 1;
	    records_amount_++;
	  } else if (key < node->key_) {
	    // После вставки высота левого поддерева увеличилась => балансировка
	    if (InsBalanceTree((TBalanceNode*&)node->node_left_, key, value) == 1)
	      height_index = LeftTreeBalancing(node);
	  } else if (key < node->key_) {
	    if (InsBalanceTree((TBalanceNode*&)node->node_right_, key, value) == 1)
	      height_index = RightTreeBalancing(node);
	  } else {
	    height_index = 0;
	  }
	
	  return height_index;
	}
	
	int TBalanceTree::LeftTreeBalancing(TBalanceNode*& node) {
	  int height_index = 0;
	
	  // Проверка предыдущей балансировки
	  switch(node->balance_) {
	    // В поддереве был перевес справа - устанавливается равновесие
	    case TBalanceNode::Balance::BALANCE_RIGHT:
	      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	      height_index = 0;
	      break;
	
	    // В поддереве было равновесие - устанавливается перевес слева
	    case TBalanceNode::Balance::BALANCE_OK:
	      node->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
	      height_index = 1;
	      break;
	
	    // В поддереве был перевес слева - необходима балансировка
	    case TBalanceNode::Balance::BALANCE_LEFT:
	      TBalanceNode *node1, *node2;
	
	      node1 = (TBalanceNode*)node->node_left_;
	
	      // Случай 1 - однократный LL-поворот
	      if (node1->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
	        node->node_left_ = node1->node_right_;
	        node1->node_right_ = node;
	        node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	        node = node1;
	      // Случай 2 - двукратный LR-поворот
	      } else {
	        node2 = (TBalanceNode*)node1->node_right_;
	        node1->node_right_ = node2->node_left_;
	        node2->node_left_ = node1;
	        node->node_left_ = node2->node_right_;
	        node2->node_right_ = node;
	
	        if (node2->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
	          node->SetBalance(TBalanceNode::Balance::BALANCE_RIGHT);
	        } else if (node2->balance_ == TBalanceNode::Balance::BALANCE_RIGHT) {
	          node1->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
	        } else {
	          node1->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	        }
	
	        node = node2;
	      }
	
	      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	      height_index = 0;
	      break;
	  }  // switch
	
	  return height_index;
	}
	
	int TBalanceTree::RightTreeBalancing(TBalanceNode*& node) {
	  int height_index = 0;
	
	  // Проверка предыдущей балансировки
	  switch(node->balance_) {
	    // В поддереве был перевес слева - устанавливается равновесие
	    case TBalanceNode::Balance::BALANCE_LEFT:
	      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	      height_index = 0;
	      break;
	
	    // В поддереве было равновесие - устанавливается перевес справа
	    case TBalanceNode::Balance::BALANCE_OK:
	      node->SetBalance(TBalanceNode::Balance::BALANCE_RIGHT);
	      height_index = 1;
	      break;
	
	    // В поддереве был перевес справа - необходима балансировка
	    case TBalanceNode::Balance::BALANCE_RIGHT:
	      TBalanceNode *node1, *node2;
	
	      node1 = (TBalanceNode*)node->node_right_;
	
	      // Случай 1 - однократный RR-поворот
	      if (node1->balance_ == TBalanceNode::Balance::BALANCE_RIGHT) {
	        node->node_right_ = node1->node_left_;
	        node1->node_left_ = node;
	        node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	        node = node1;
	      // Случай 2 - двукратный RL-поворот
	      } else {
	        node2 = (TBalanceNode*)node1->node_left_;
	        node1->node_left_ = node2->node_right_;
	        node2->node_right_ = node1;
	        node->node_right_ = node2->node_left_;
	        node2->node_left_ = node;
	
	        if (node2->balance_ == TBalanceNode::Balance::BALANCE_RIGHT) {
	          node->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
	        } else if (node2->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
	          node1->SetBalance(TBalanceNode::Balance::BALANCE_RIGHT);
	        } else {
	          node1->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	        }
	
	        node = node2;
	      }
	
	      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
	      height_index = 0;
	      break;
	  }  // switch
	
	  return height_index;
	}

Модульное тестирование - *test_tbalancetree.h*

	#include <gtest/gtest.h>
	
	#include "include/TBalanceTree.h"
	#include "include/TWord.h"
	
	TEST(BakalinKazakov_TBalanceTree, Can_Insert_Record) {
	  TBalanceTree table;
	  TWord* word = new TWord("Multiprogramm");
	
	  ASSERT_NO_THROW(table.InsRecord("ab", word));
	}
	
	TEST(BakalinKazakov_TBalanceTree, Can_Find_Record) {
	  TBalanceTree table;
	  TWord* word1 = new TWord("Multiprogramm");
	  TWord* word2 = new TWord("SGBash");
	  table.InsRecord("aa", word1);
	  table.InsRecord("ab", word2);
	
	  TWord* result = (TWord*)table.FindRecord("ab");
	
	  EXPECT_EQ(result->GetWord(), "SGBash");
	}
	
	TEST(BakalinKazakov_TBalanceTree, Can_Delete_Record) {
	  TBalanceTree table;
	  TWord* word1 = new TWord("Multiprogramm");
	  TWord* word2 = new TWord("SGBash");
	  table.InsRecord("aa", word1);
	  table.InsRecord("ab", word2);
	
	  table.DelRecord("ab");
	  TWord* result = (TWord*)table.FindRecord("ab");
	
	  EXPECT_EQ(result, nullptr);
	}

###2.10 Результаты модульного тестирования

![](test.png)

###2.11 Пример приложения

Код *main.cpp*

	#include "include\TScanTable.h"
	#include "include\TWord.h"
	#include <iostream>
	#include <string>
	
	using namespace std;
	
	int main() {
	  TScanTable table(7);
	
	  TWord* excellent = new TWord("Excellent");
	  TWord* good = new TWord("Good");
	  TWord* satisfactory = new TWord("Satisfactory");
	  TWord* unsatisfactory = new TWord("Unsatisfactory");
	
	  table.InsRecord("Ivanov", good);
	  table.InsRecord("Kozlov", good);
	  table.InsRecord("Kiselev", satisfactory);
	  table.InsRecord("Zhorin", unsatisfactory);
	  table.InsRecord("Sergeev", excellent);
	  table.InsRecord("Saharov", unsatisfactory);
	  table.InsRecord("Zheludev", satisfactory);
	
	  cout << "Table printing" << endl;
	
	  for (table.Reset(); !table.IsTabEnded(); table.GoNext()) {
	    cout << "Key: " << table.GetKey() << ", Value: " << *(TWord*)table.GetValue() << endl;
	  }
	
	  TWord* result = (TWord*)table.FindRecord("Zhorin");
	
	  cout << "Find Zhorin" << endl;
	  cout << *result << endl;
	
	  return 0;
	}

Результат

![](main.png)

##3. Вывод

В ходе выполнения данной работы были получены навыки создания различных типов таблиц, изучены их особенности и ключевые моменты в их реализации. 

Было проведено модульное тестирование разработанных классов при помощи Google Test Framework. Тесты показали, что разработанная программа успешно решает поставленную в начале работы задачу.